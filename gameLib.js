function Ship() {
    tShip = new Sprite(game, "ship.png", 500, 600);
    tShip.loadAnimation(500, 600, 125, 150);
    tShip.generateAnimationCycles();
    tShip.renameCycles(new Array("down", "left", "right", "up"));
    tShip.setAnimationSpeed(500);

    //start paused
    tShip.setPosition(400, 350);
    tShip.setSpeed(0);
    tShip.pauseAnimation();
    tShip.setCurrentCycle("up");

    tShip.checkKeys = function () {

        if (keysDown[K_LEFT] || keysDown[K_A]) {
            tShip.setSpeed(5);
            tShip.playAnimation()
            tShip.setMoveAngle(270);
            tShip.setCurrentCycle("left");
        }
        if (keysDown[K_RIGHT] || keysDown[K_D]) {
            tShip.setSpeed(5);
            tShip.playAnimation()
            tShip.setMoveAngle(90);
            tShip.setCurrentCycle("right");
        }
        if (keysDown[K_UP] || keysDown[K_W]) {
            tShip.setSpeed(5);
            tShip.playAnimation()
            tShip.setMoveAngle(0);
            tShip.setCurrentCycle("up");
        }
        if (keysDown[K_DOWN] || keysDown[K_S]) {
            tShip.setSpeed(5);
            tShip.playAnimation()
            tShip.setMoveAngle(180);
            tShip.setCurrentCycle("down");
        }
        if (keysDown[K_SPACE]) {
            tShip.setSpeed(0);
            tShip.pauseAnimation();
            tShip.setCurrentCycle("up");
        }

    }

    return tShip;

}

function Treasure() {
    tTreasure = new Sprite(game, "box.png", 50, 60);

    tTreasure.setSpeed(20);

    tTreasure.motionTreasure = function () {
        newDir = (Math.random() * 70) - 35;
        this.changeAngleBy(newDir);
    }

    tTreasure.collect = function () {
        newX = Math.random() * this.cWidth;
        newY = Math.random() * this.cHeight;
        this.setPosition(newX, newY);
    }

    return tTreasure;

}

function Bomb() {
    tBomb = new Sprite(game, "bomb.png", 70, 55);
    tBomb.setSpeed(4);

    tBomb.motionBomb = function () {
        newDir = (Math.random() * 70) - 35;
        this.changeAngleBy(newDir);
    }

    return tBomb;

}